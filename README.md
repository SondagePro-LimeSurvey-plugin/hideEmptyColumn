# hideEmptyColumn #

Hide the empty column in LimeSurvey. Demonstration of usage : <https://demonstration.sondages.pro/655577>

## Installation

This plugin is tested with LimeSurvey 2.06, and 2.50. In 2.50 here are still some cosmectic issue for the width of some column.

### Via GIT
- Go to your LimeSurvey Directory (version up to 2.06 only)
- Clone in plugins/hideEmptyColumn directory `git clone https://framagit.org/SondagePro-LimeSurvey-plugin/hideEmptyColumn.git hideEmptyColumn`

### Via ZIP dowload
- Download <https://extensions.sondages.pro/IMG/auto/hideEmptyColumn.zip>
- Extract : `unzip hideEmptyColumn.zip`
- Move the directory to  plugins/ directory inside LimeSUrvey

## Home page & Copyright
- HomePage <https://extensions.sondages.pro/rubrique5>
- Copyright © 2015-2016 Denis Chenu <https://sondages.pro>
- Copyright © 2014 Incidence <http://www.incidence.be/>

Distributed under [GNU GENERAL PUBLIC LICENSE Version 3](https://gnu.org/licenses/gpl-3.0.txt) licence
